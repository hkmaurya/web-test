import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from "../../environments/environment";
@Injectable({ providedIn: 'root' })
export class AuthService {
    private currentUserSubject: BehaviorSubject<any>;
    public currentUser: Observable<any>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('sabKharidoUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue() {       
        return this.currentUserSubject.value;
    }

    login(opts) {
        return this.http.post<any>(`${environment.apiUrl}user/login`, opts)
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('sabKharidoUser', JSON.stringify(user));
                this.currentUserSubject.next(user);
                return user;
            }));
    }

    signUp(opts) {
        return this.http.post<any>(`${environment.apiUrl}user/signup`, opts)
    }

    logout() {
        // remove user from local storage and set current user to null
        localStorage.removeItem('sabKharidoUser');
        this.currentUserSubject.next(null);
    }
}
