import { Injectable,Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { Subject } from 'rxjs';
import { AuthService } from "./auth.service";
@Injectable({
  providedIn: 'root'
})
export class UserService {
  public httpOptions: any;
  public formDataOptions: any;
  public token: any;
  constructor(private http: HttpClient, @Inject(PLATFORM_ID) private platformId: any, public authService : AuthService) { }

  GetHttpHeaders(): HttpHeaders {
    this.token = this.authService.currentUserValue ? this.authService.currentUserValue.token : '';
    if (isPlatformBrowser(this.platformId)) {
      const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.token}`,
        'privatekey': environment.key.privateKey
      })
      return headers
    }
  }

  GetHttpHeaderFormData(): HttpHeaders {
    this.token = this.authService.currentUserValue ? this.authService.currentUserValue.token : '';
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`,
      'privatekey': environment.key.privateKey
    })
    return headers
  }

  getMyProducts(searchData) {
    let params = new HttpParams();
    if(searchData){
      params = params.append('limit', searchData.limit)
      params = params.append('offset', searchData.offset)
    }
    return this.http.get(`${environment.apiUrl}/user/products`, { headers: this.GetHttpHeaders(), params })
  }

  getMyProductDetail(id){
    return this.http.get(`${environment.apiUrl}/user/product/${id}`, { headers: this.GetHttpHeaders() })
  }
  
  updateMyProduct(opts, id){
    return this.http.put(`${environment.apiUrl}/user/product/${id}`, opts, { headers: this.GetHttpHeaders() })
  }

  deleteMyProduct(id){
    return this.http.delete(`${environment.apiUrl}/user/myproduct/${id}`, { headers: this.GetHttpHeaders() })
  }

  addMyProductLocation(opts) {
    return this.http.put(`${environment.apiUrl}/user/product/location`, opts, { headers: this.GetHttpHeaders() })
  }

  removeMyProductLocation(productId, locationId) {
    return this.http.delete(`${environment.apiUrl}/user/product/${productId}/location/${locationId}`, { headers: this.GetHttpHeaders() })
  }
}
