import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { Subject } from 'rxjs';
import { AuthService } from "./auth.service";
@Injectable({
  providedIn: 'root'
})
export class AdminService {
  public httpOptions: any;
  public formDataOptions: any;
  public token: any;
  constructor(private http: HttpClient, @Inject(PLATFORM_ID) private platformId: any, public authService : AuthService) {
    
  }

  GetHttpHeaders(): HttpHeaders {
    this.token = this.authService.currentUserValue ? this.authService.currentUserValue.token : '';
    if (isPlatformBrowser(this.platformId)) {
      const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${JSON.parse(localStorage.getItem('sabKharidoUser')).token}`,
        'accept-language': 'en'
      })
      return headers
    }
  }

  GetHttpHeaderFormData(): HttpHeaders {
    this.token = this.authService.currentUserValue ? this.authService.currentUserValue.token : '';
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${JSON.parse(localStorage.getItem('sabKharidoUser')).token}`,
      'privatekey': environment.key.privateKey
    })
    return headers
  }

  getMenu() {
    return this.http.get(`${environment.apiUrl}/menus`, { headers: this.GetHttpHeaders() })
  }

  addMenu(opts) {
    return this.http.post(`${environment.apiUrl}/menu`, opts, { headers: this.GetHttpHeaders() })
  }

  getMenuDetail(id) {
    return this.http.get(`${environment.apiUrl}/menu/` + id, { headers: this.GetHttpHeaders() })
  }

  updateMenu(opts, id) {
    return this.http.put(`${environment.apiUrl}/menu/${id}`, opts, { headers: this.GetHttpHeaders() })
  }

  getCategories(searchData) {
    let params = new HttpParams();
    if(searchData){
      params = params.append('limit', searchData.limit)
      params = params.append('offset', searchData.offset)
    }
    if(searchData.menuId){
      params = params.append('menu', searchData.menuId)
    }
    if(searchData.title){
      params = params.append('title', searchData.title)
    }
    return this.http.get(`${environment.apiUrl}/categories`, { headers: this.GetHttpHeaders(), params })
  }

  getSubCategories(searchData, id) {
    let params = new HttpParams();
    if(searchData){
      params = params.append('limit', searchData.limit)
      params = params.append('offset', searchData.offset)
    }
    if(searchData.title){
      params = params.append('title', searchData.title)
    }
    return this.http.get(`${environment.apiUrl}/category/${id}/subcategories`, { headers: this.GetHttpHeaders(), params })
  }

  getSubCategoryDetail(categoryId, subCategoryId){
    return this.http.get(`${environment.apiUrl}/category/${categoryId}/subcategory/${subCategoryId}`, { headers: this.GetHttpHeaders() })
  }

  deleteCategories(id) {
    return this.http.delete(`${environment.apiUrl}/category/${id}`, { headers: this.GetHttpHeaders() })
  }

  deleteSubCategories(id){
    return this.http.delete(`${environment.apiUrl}/subcategory/${id}`, { headers: this.GetHttpHeaders() })
  }
  
  addCategory(opts) {
    let categoryPicture = new FormData();
    categoryPicture.append("picture", opts.picture)
    categoryPicture.append("title", opts.title)
    categoryPicture.append("description", opts.description)
    categoryPicture.append("menu", opts.menu)
    return this.http.post(`${environment.apiUrl}/category`, categoryPicture, { headers: this.GetHttpHeaderFormData() })
  }

  getCategoryDetail(id) {
    return this.http.get(`${environment.apiUrl}/category/` + id, { headers: this.GetHttpHeaders() })
  }

  updateCategory(opts, id) {
    let categoryPicture = new FormData();
    if(opts.picture){
      categoryPicture.append("picture", opts.picture)
    }
    categoryPicture.append("title", opts.title)
    categoryPicture.append("description", opts.description)
    categoryPicture.append("menu", opts.menu)
    return this.http.put(`${environment.apiUrl}/category/${id}`, categoryPicture, { headers: this.GetHttpHeaderFormData() })
  }

  addSubCategory(opts) {
    let categoryPicture = new FormData();
    categoryPicture.append("picture", opts.picture)
    categoryPicture.append("title", opts.title)
    categoryPicture.append("description", opts.description)
    categoryPicture.append("categoryId", opts.categoryId)
    return this.http.post(`${environment.apiUrl}/subcategory`, categoryPicture, { headers: this.GetHttpHeaderFormData() })
  }

  updateSubCategory(opts, id){
    let categoryPicture = new FormData();
    if(opts.picture){
      categoryPicture.append("picture", opts.picture)
    }
    categoryPicture.append("title", opts.title)
    categoryPicture.append("description", opts.description)
    categoryPicture.append("categoryId", opts.categoryId)
    return this.http.put(`${environment.apiUrl}/subcategory/${id}`, categoryPicture, { headers: this.GetHttpHeaderFormData() })
  }

  getProducts() {
    return this.http.get(`${environment.apiUrl}products`, { headers: this.GetHttpHeaders() })
  }

  addProduct(opts) {
    return this.http.post(`${environment.apiUrl}product`, opts, { headers: this.GetHttpHeaders() })
  }

  addProductLocation(opts) {
    return this.http.put(`${environment.apiUrl}/product/location`, opts, { headers: this.GetHttpHeaders() })
  }

  removeProductLocation(productId, locationId) {
    return this.http.delete(`${environment.apiUrl}/product/${productId}/location/${locationId}`, { headers: this.GetHttpHeaders() })
  }

  updateProduct(id, opts) {
    return this.http.put(`${environment.apiUrl}product/` + id, opts, { headers: this.GetHttpHeaders() })
  }

  addProductImage(opts) {
    let categoryPicture = new FormData();
    categoryPicture.append("productId", opts.productId)
    categoryPicture.append("isDefault", opts.isDefault)
    categoryPicture.append("picture", opts.picture)    
    return this.http.put(`${environment.apiUrl}/product/image`, categoryPicture, { headers: this.GetHttpHeaderFormData() })
  }

  removeProductImage(productId, imageId){
    return this.http.delete(`${environment.apiUrl}/product/${productId}/image/${imageId}`, { headers: this.GetHttpHeaders() })
  }

  getProductDetail(id) {
    return this.http.get(`${environment.apiUrl}product/` + id, { headers: this.GetHttpHeaders() })
  }

  addSimilarProduct(opts) {
    return this.http.put(`${environment.apiUrl}/similar/product`, opts, { headers: this.GetHttpHeaders() })
  }

  deleteProduct(id) {
    return this.http.delete(`${environment.apiUrl}/product/` + id, { headers: this.GetHttpHeaders() })
  }

  deleteSimilarProduct(id, similarId) {
    return this.http.delete(`${environment.apiUrl}/product/` + id + '/similar/' + similarId, { headers: this.GetHttpHeaders() })
  }

  boughtTogether(opts) {
    return this.http.put(`${environment.apiUrl}/bought/together/product`, opts, { headers: this.GetHttpHeaders() })
  }

  deleteboughtTogetherProduct(id, deleteboughtTogetherId) {
    return this.http.delete(`${environment.apiUrl}/product/` + id + '/bought/together/' + deleteboughtTogetherId, { headers: this.GetHttpHeaders() })
  }

  addProductVariant(opts) {
    return this.http.put(`${environment.apiUrl}/variant/product`, opts, { headers: this.GetHttpHeaders() })
  }

  deleteProductVariant(id, variantProductId) {
    return this.http.delete(`${environment.apiUrl}/product/` + id + '/variant/' + variantProductId, { headers: this.GetHttpHeaders() })
  }

  getZipcode(){
    return this.http.get(`${environment.apiUrl}/zipcodes`, { headers: this.GetHttpHeaders() })
  }

  addZipcode(opts){
    return this.http.post(`${environment.apiUrl}/zipcode`, opts, { headers: this.GetHttpHeaders() })
  }

  deleteZipcode(id){
    return this.http.delete(`${environment.apiUrl}/zipcode/${id}`, { headers: this.GetHttpHeaders() })
  }

  getUsers(){
    return this.http.get(`${environment.apiUrl}users`, { headers: this.GetHttpHeaders() })
  }

  getUserDetail(id){
    return this.http.get(`${environment.apiUrl}/user/${id}`, { headers: this.GetHttpHeaders() })
  }

  addUser(opts){
    return this.http.post(`${environment.apiUrl}/profile`, opts, { headers: this.GetHttpHeaders() })
  }

  updateUser(opts, id){
    return this.http.put(`${environment.apiUrl}/profile?user=${id}`, opts, { headers: this.GetHttpHeaders() })
  }

  deleteUser(user){
    return this.http.delete(`${environment.apiUrl}/profile/${user}`, { headers: this.GetHttpHeaders() })
  }

  assignDestributorMenu(opts){
    return this.http.put(`${environment.apiUrl}/user/menu/access`, opts, { headers: this.GetHttpHeaders() })
  }

  deleteDestributorMenu(userId, menuId){
    return this.http.delete(`${environment.apiUrl}/user/${userId}/menu/${menuId}`, { headers: this.GetHttpHeaders() })
  }

  addUserProfilePicture(opts, userId){
    let profilePicture = new FormData();
    profilePicture.append("picture", opts.picture)
    let url = userId ? `${environment.apiUrl}/picture/upload?user=${userId}` : `${environment.apiUrl}/picture/upload` ;
    return this.http.put(url, profilePicture, { headers: this.GetHttpHeaderFormData() })
  }

  addSize(size){
    return this.http.post(`${environment.apiUrl}/size`, size, { headers: this.GetHttpHeaders() })
  }

  updateSize(id, opts){
    return this.http.put(`${environment.apiUrl}/size/${id}`, opts, { headers: this.GetHttpHeaders() })
  }

  getSizes(){
    return this.http.get(`${environment.apiUrl}/sizes`, { headers: this.GetHttpHeaders() })
  }

  getSizeDetail(id){
    return this.http.get(`${environment.apiUrl}/size/${id}`, { headers: this.GetHttpHeaders() })
  }

  addColor(color){
    return this.http.post(`${environment.apiUrl}/color`, color, { headers: this.GetHttpHeaders() })
  }

  updateColor(id, opts){
    return this.http.put(`${environment.apiUrl}/color/${id}`, opts, { headers: this.GetHttpHeaders() })
  }

  getColors(){
    return this.http.get(`${environment.apiUrl}/colors`, { headers: this.GetHttpHeaders() })
  }

  getColorDetail(id){
    return this.http.get(`${environment.apiUrl}/color/${id}`, { headers: this.GetHttpHeaders() })
  }

  addWeight(weight){
    return this.http.post(`${environment.apiUrl}/weight`, weight, { headers: this.GetHttpHeaders() })
  }

  updateWeight(id, opts){
    return this.http.put(`${environment.apiUrl}/weight/${id}`, opts, { headers: this.GetHttpHeaders() })
  }

  getWeights(){
    return this.http.get(`${environment.apiUrl}/weights`, { headers: this.GetHttpHeaders() })
  }

  getWeightDetail(id){
    return this.http.get(`${environment.apiUrl}/weight/${id}`, { headers: this.GetHttpHeaders() })
  }

  addMyProduct(opts) {
    return this.http.post(`${environment.apiUrl}/user/product`, opts, { headers: this.GetHttpHeaders() })
  }

  getBrands(searchData) {
    let params = new HttpParams();
    if(searchData){
      params = params.append('limit', searchData.limit)
      params = params.append('offset', searchData.offset)
    }
    if(searchData.title){
      params = params.append('title', searchData.title)
    }
    return this.http.get(`${environment.apiUrl}/brands`, { headers: this.GetHttpHeaders(), params })
  }

  getBrandDetail(id) {
    return this.http.get(`${environment.apiUrl}/brand/${id}`, { headers: this.GetHttpHeaders() })
  }

  getSelectedBrand(searchData) {
    let params = new HttpParams();
    if(searchData){
      params = params.append('limit', searchData.limit)
      params = params.append('offset', searchData.offset)
    }
    return this.http.get(`${environment.apiUrl}/brands/subcategories?subCategoryId=${searchData.subcategory}`, { headers: this.GetHttpHeaders(), params })
  }

  removeSelectedBrand(id) {
    return this.http.delete(`${environment.apiUrl}/brand/subcategory/${id}`, { headers: this.GetHttpHeaders() })
  }

  addBrand(opts) {
    return this.http.post(`${environment.apiUrl}/brand`, opts, { headers: this.GetHttpHeaders() })
  }

  updateBrand(opts, id) {
    return this.http.put(`${environment.apiUrl}/brand/${id}`, opts, { headers: this.GetHttpHeaders() })
  }

  setBrandInSubcategory(opts){
    return this.http.put(`${environment.apiUrl}/brand/subcategory`, opts, { headers: this.GetHttpHeaders() })
  }

}
