import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { AuthGuard } from "../core/auth.guard";

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        canActivate : [AuthGuard]
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./users/users.module').then(m => m.UsersModule),
        canActivate : [AuthGuard]
      },
      {
        path: 'product',
        data: {
          breadcrumb: 'Product'
        },
        loadChildren: () => import('./product/product.module').then(m => m.ProductModule),
        canActivate : [AuthGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
