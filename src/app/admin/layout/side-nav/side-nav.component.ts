import { Component, OnInit } from '@angular/core';
import { AdminService, AuthService } from "../../../service";
import { Router } from "@angular/router";
import { Subject } from 'rxjs';
@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {
  currentUser: any
  constructor(public adminService: AdminService, public router: Router, public authService: AuthService) { }

  ngOnInit() {
    this.currentUser = this.authService.currentUserValue.data
  }
}
