import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductComponent } from './product.component';
import { AddComponent } from '../product/add/add.component';
import { DetailComponent } from '../product/detail/detail.component';

const routes: Routes = [
  {
    path: '',
    component: ProductComponent
  },  
  {
    path: 'add',
    data: {
      breadcrumb: 'Add Product'
    },
    component: AddComponent
  }, 
  {
    path: 'detail/:id',
    data: {
      breadcrumb: 'Product detail'
    },
    component: DetailComponent
  }, 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
