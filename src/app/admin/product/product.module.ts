import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product.component';
import { ProductRoutingModule } from "./product-routing.module";
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatGridListModule } from '@angular/material/grid-list';
import { AddComponent } from './add/add.component';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { HttpClientModule } from '@angular/common/http';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { DetailComponent } from './detail/detail.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MatBadgeModule } from '@angular/material/badge';
import { SharedModule } from "../../shared/shared.module";
import { MatChipsModule } from '@angular/material/chips';
import { CKEditorModule } from 'ngx-ckeditor';
@NgModule({
  declarations: [ProductComponent, AddComponent, DetailComponent],
  imports: [
    CommonModule,
    ProductRoutingModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FormsModule,
    MatButtonModule,
    MatCardModule,
    MatTableModule,
    MatGridListModule,
    MatInputModule,
    MatSelectModule,
    HttpClientModule,
    AngularEditorModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    NgxMatSelectSearchModule,
    MatBadgeModule,
    SharedModule,
    MatChipsModule,
    MatIconModule,
    CKEditorModule
  ]
})
export class ProductModule { }
