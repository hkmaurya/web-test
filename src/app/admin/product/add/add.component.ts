import { Component, OnInit } from '@angular/core';
import { AdminService, AlertService } from "../.../../../../service";
import { FormGroup, FormControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { take, takeUntil } from 'rxjs/operators';
import { ReplaySubject, Subject } from 'rxjs';
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  productForm: FormGroup;
  products: Array<any> = [];
  productDetail: any;
  routeObj: any;
  productID: any;
  goToBack: boolean = false;
  processRequest: boolean = false;
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '500',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter description...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '14',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' }
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      ['bold', 'italic'],
      ['fontSize']
    ]
  };
  listIndex: any;
  constructor(public adminService: AdminService, public router: Router, private route: ActivatedRoute, public alertService: AlertService) { }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(params => {
      this.routeObj = { ...params.keys, ...params };
      this.productID = this.routeObj.params.id;
      this.listIndex = this.routeObj.params.index;
      if (this.productID) {
        this.getPrdouctDetail(this.productID)
      }
    });
    this.createProductForm();
    this.getProducts();
  }

  getProducts() {
    this.adminService.getProducts().subscribe(data => {
      this.products = data['data'];
      this.products = this.products.filter(product => {
        product.title = product.title.toUpperCase()
        return product._id != this.productID
      })
    }, error => {

    })
  }


  getPrdouctDetail(id) {
    this.adminService.getProductDetail(id).subscribe(data => {
      this.productDetail = data['data'];
      this.productForm.setValue({
        title: data['data']['title'],
        price: data['data']['price'],
        description: data['data']['description'],
        shortDescription: data['data']['shortDescription'],
        quantity: data['data'].quantity
      })
    })
  }

  createProductForm() {
    this.productForm = new FormGroup({
      title: new FormControl(''),
      description: new FormControl(''),
      shortDescription: new FormControl(''),
      price: new FormControl(''),
      quantity: new FormControl('')
    });
  }

  addProduct(data) {
    this.processRequest = true;
    let opts = {
      title: data.title,
      description: data.description,
      shortDescription: data.shortDescription,
      price: data.price,
      quantity: data.quantity
    }
    if (!this.goToBack) {
      if (!this.productID) {
        this.adminService.addProduct(opts).subscribe(product => {
          this.processRequest = false;
          this.alertService.success(data['message'])
          this.router.navigate(['/product'])
        }, err => {
          this.processRequest = false;
          this.alertService.error(err['message'])
        })
      } else {
        this.adminService.updateProduct(this.productID, opts).subscribe(product => {
          this.processRequest = false;
          this.alertService.success(product['message'])
          this.router.navigate(['/product'])
        }, error => {
          this.processRequest = false;
          this.alertService.error(error['message'])
        })
      }
    }
  }

  goToPrevious() {
    this.router.navigate(['/product'])
    this.goToBack = true;
  }
}
