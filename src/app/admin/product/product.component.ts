import { Component, OnInit } from '@angular/core';
import { AdminService, AuthService } from "../../service";
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  public products: Array<any> = [];
  public dataLoader: boolean = true;
  productTitle: string;
  getProductData: Subscription;
  currentUser: any;
  constructor(public adminService: AdminService, public authService: AuthService, public router: Router, public activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.currentUser = this.authService.currentUserValue.data
    this.getProducts()
  }

  getProducts() {
    this.dataLoader = true;
    this.adminService.getProducts().subscribe(data => {
      this.products = data['data']
      this.dataLoader = false;
    }, error => {
      this.dataLoader = false;
    })
  }

  goToAddProduct() {
    this.router.navigate(['/product/add'])
  }

  editProduct(id) {
    this.router.navigate(['/product/add'], { queryParams: { id: id } })
  }

  viewProduct(id) {
    this.router.navigate([`/product/detail/${id}`])
  }
  deleteProduct(id) {
    let result = prompt("Please enter DELETE to delete");
    if (result == 'DELETE') {
      this.adminService.deleteProduct(id).subscribe(data => {
        this.getProducts();
      }, error => {

      })
    }
  }

}
