import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup,  FormControl } from "@angular/forms";
import { AuthService } from "../../service/auth.service";
import { environment } from "../../../environments/environment";
import { PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(private router: Router, public authService : AuthService, @Inject(PLATFORM_ID) private platformId: any) {}
  loginForm:FormGroup
  signupForm:FormGroup
  isLogin:boolean = true
  ngOnInit() {
    this.createLoginForm()
    this.createSignUpForm()
  }

  createLoginForm(){
    this.loginForm = new FormGroup({
      email: new FormControl(''),
      password: new FormControl(''),
    });
  }

  createSignUpForm(){
    this.signupForm = new FormGroup({
      firstName: new FormControl(''),
      lastName: new FormControl(''),
      email: new FormControl(''),
      password: new FormControl(''),
    });
  }

  onLogin(loginData) {
    let opts = {
      password : loginData.password,
      email : loginData.email
    }
    this.authService.login(opts).subscribe((data) => {
        this.router.navigate(['/dashboard']);
        if (isPlatformBrowser(this.platformId)) {
          localStorage.setItem('isLoggedin', 'true');
        }
      })    
  }

  onSignup(signupData) {
    let opts = {
      firstName : signupData.firstName,
      lastName : signupData.lastName,
      password : signupData.password,
      email : signupData.email
    }
    this.authService.signUp(opts).subscribe((data) => {
        this.isLogin = true
      })  
  }
}
